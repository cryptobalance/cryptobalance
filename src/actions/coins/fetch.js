export const FETCH_COINS = 'FETCH_COINS'
export const LOAD_ERROR = 'LOAD_ERROR'

export default () => {
  return {
      type: FETCH_COINS
    }
}